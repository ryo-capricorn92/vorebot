const possibleBreaks = [' ', '.', ',', '/', '<', '>', '?', '`', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '+', '=', '|'];

const possibleReplacements = {
  v: ['ᵥ', 'ᵛ', 'ṽ', 'ṿ', 'ⅴ', '⒱', 'ⓥ', 'ｖ', '𝐯', '𝑣', '𝒗', '𝓋', '𝓿', '𝔳', '𝕧', '𝖛', '𝗏', '𝘃', '𝘷', '𝙫', '𝚟'],
  o: ['º', 'ò', 'ó', 'ô', 'õ', 'ö', 'ō', 'ŏ', 'ő', 'ơ', 'ǒ', 'ǫ', 'ȍ', 'ȏ', 'ȯ', 'ᵒ', 'ọ', 'ỏ', 'ₒ', 'ℴ', '⒪', 'ⓞ', 'ｏ', '𝐨', '𝑜', '𝒐', '𝓸', '𝔬', '𝕠', '𝖔', '𝗈', '𝗼', '𝘰', '𝙤', '𝚘'],
  r: ['ŕ', 'ŗ', 'ř', 'ȑ', 'ȓ', 'ʳ', 'ᵣ', 'ṙ', 'ṛ', 'ṟ', '⒭', 'ⓡ', 'ｒ', '𝐫', '𝑟', '𝒓', '𝓇', '𝓻', '𝔯', '𝕣', '𝖗', '𝗋', '𝗿', '𝘳', '𝙧', '𝚛'],
  e: ['è', 'é', 'ê', 'ë', 'ē', 'ĕ', 'ė', 'ę', 'ě', 'ȅ', 'ȇ', 'ȩ', 'ᵉ', 'ḙ', 'ḛ', 'ẹ', 'ẻ', 'ẽ', 'ₑ', 'ℯ', 'ⅇ', '⒠', 'ⓔ', 'ｅ', '𝐞', '𝑒', '𝒆', '𝓮', '𝔢', '𝕖', '𝖊', '𝖾', '𝗲', '𝘦', '𝙚', '𝚎'],
};

const replace = (str, char, cleanChar) => {
  const copy = str.split('');
  const index = str.indexOf(char);
  if (index >= 0) { copy[index] = cleanChar; }
  return copy.join('');
};

module.exports = (message) => {
  const mergedInput = possibleBreaks.reduce((str, char) => str.split(char).join(''), message);
  const testing = Object.keys(possibleReplacements).reduce((string, letter) => (
    possibleReplacements[letter].reduce((str, char) => replace(str, char, letter), string)
  ), mergedInput);
  return testing;
};
