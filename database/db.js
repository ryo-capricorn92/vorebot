const Sequelize = require('sequelize');

const options = process.env.NODE_ENV === 'production' ? {
  ssl: true,
  dialectOptions: {
    ssl: {
      require: true,
    },
  },
} : {
  logging: false,
};

const db = new Sequelize(process.env.DATABASE_URL, options);

module.exports = db;
