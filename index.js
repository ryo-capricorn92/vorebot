const Discord = require('discord.js');
const dotenv = require('dotenv');
const path = require('path');

if (process.env.NODE_ENV !== 'production') {
  dotenv.config({ path: path.join(__dirname, '.local.env') });
}

const db = require('./database/db');
const clean = require('./src/clean');

const client = new Discord.Client();

// config
const token = process.env.DISCORD_TOKEN;
const prefix = 'v!';

client.on('ready', () => {
  /* eslint-disable-next-line no-console */
  console.log(`
    Bot has started, with ${client.users.size} users, 
    in ${client.channels.size} channels of ${client.guilds.size} guilds.
  `);

  client.user.setActivity('with heathens');
});

client.on('message', async (message) => {
  if (message.author.bot) { return; }

  // if (message.content.indexOf(prefix) !== 0) { return; }

  const cleanInput = clean(message.content);

  if (cleanInput.includes('vore')) {
    message.channel.send('YOU SAID VORE');
  }
});

db.sync().then(() => {
  client.login(token);
});
